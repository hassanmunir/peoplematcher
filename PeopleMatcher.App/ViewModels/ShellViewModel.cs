using System;
using System.Security.Policy;
using System.Windows;
using Caliburn.Micro;
using PeopleMatcher.App.Mocks;
using PeopleMatcher.Core;

namespace PeopleMatcher.App.ViewModels
{
    public class ShellViewModel : Conductor<IScreen>.Collection.OneActive, IShell
    {

        public ShellViewModel()
        {
            var peopleRepository = new MockPeopleRepository();
            
            Items.Add(new PeopleViewModel(peopleRepository, new ApplicationSettings() ));
            Items.Add(new MatchesViewModel(new MockMatchesRepository(peopleRepository)));

            ActiveItem = Items[0];
        }
    }

    public class Model
    {
        public Model()
        { 
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
    }
}