﻿using System.Collections.ObjectModel;
using Caliburn.Micro;
using PeopleMatcher.Core;
using PeopleMatcher.Core.Extensions;
using PeopleMatcher.Core.Models;

namespace PeopleMatcher.App.ViewModels
{
    public class MatchesViewModel : Screen
    {
        readonly IMatchRepository _matchRepository;

        public MatchesViewModel(IMatchRepository matchRepository)
        {
            _matchRepository = matchRepository;
            Matches = new BindableCollection<Match>();
            SelectedMatches = new BindableCollection<Match>();
            initialiseMatches();
        }

        public override string DisplayName => "Matches";
        public BindableCollection<Match> Matches { get; set; }
        public BindableCollection<Match> SelectedMatches { get; set; }

        void initialiseMatches()
        {
            _matchRepository.GetAll().ForEach(Matches.Add);
        }
    }
}