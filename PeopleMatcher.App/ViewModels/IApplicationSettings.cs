﻿using System.ComponentModel;

namespace PeopleMatcher.App.ViewModels
{
    public interface IApplicationSettings
    {
        string ExistingMatchesFile { get; set; }
        string InputFile { get; set; }
        string NewMatchesFile { get; set; }
        event PropertyChangedEventHandler SettingsChanged;
    }
}