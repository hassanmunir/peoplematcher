﻿using System.Collections.Generic;
using Caliburn.Micro;
using PeopleMatcher.Core;
using PeopleMatcher.Core.Extensions;
using PeopleMatcher.Core.Models;
using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace PeopleMatcher.App.ViewModels
{
    public class PeopleViewModel : Screen
    {
        static readonly ILog Log = LogManager.GetLogger(nameof(PeopleViewModel));
        readonly IPeopleRepository _peopleRepository;

        public PeopleViewModel(
            IPeopleRepository peopleRepository,
            IApplicationSettings appSettings)
        {
            _peopleRepository = peopleRepository;
            People = new BindableCollection<Person>();
            SelectedPeople = new BindableCollection<Person>();
            initialisePeople();
        }

        public override string DisplayName => "People";
        public BindableCollection<Person> People { get; set; }

        public BindableCollection<Person> SelectedPeople { get; set; }

        public bool CanDelete => SelectedPeople != null && SelectedPeople.Count > 0;

        public void Delete(IEnumerable<Person> peopleToDelete)
        {
            People.RemoveRange(peopleToDelete);
        }

        void initialisePeople()
        {
            People.Clear();
            _peopleRepository.GetAll().ForEach(People.Add);
        }
    }
}