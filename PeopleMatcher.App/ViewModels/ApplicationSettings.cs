﻿using System.ComponentModel;

namespace PeopleMatcher.App.ViewModels
{
    public class ApplicationSettings : ViewModelBase, IApplicationSettings
    {
        string _existingMatchesFile;
        string _inputFile;

        public ApplicationSettings()
        {
            PropertyChanged += (sender, args) => OnSettingsChanged(args);
        }

        public string InputFile
        {
            get { return _inputFile; }
            set
            {
                if (value == _inputFile) return;
                _inputFile = value;
                OnPropertyChanged();
            }
        }

        public string ExistingMatchesFile
        {
            get { return _existingMatchesFile; }
            set
            {
                if (value == _existingMatchesFile) return;
                _existingMatchesFile = value;
                OnPropertyChanged();
            }
        }

        public string NewMatchesFile { get; set; }
        public event PropertyChangedEventHandler SettingsChanged;

        protected virtual void OnSettingsChanged(PropertyChangedEventArgs e)
        {
            SettingsChanged?.Invoke(this, e);
        }
    }
}