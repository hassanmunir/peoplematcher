﻿using System.Windows;
using Caliburn.Metro;
using Caliburn.Micro;
using PeopleMatcher.App.ViewModels;

namespace PeopleMatcher.App
{
    public class BootStrapper : CaliburnMetroCompositionBootstrapper<AppViewModel>
    {
    }

    public class AppViewModel
    {
    }
}