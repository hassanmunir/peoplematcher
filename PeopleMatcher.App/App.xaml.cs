﻿using System.Windows;
using log4net;
using log4net.Appender;
using log4net.Core;
using log4net.Layout;
using log4net.Repository.Hierarchy;
using PeopleMatcher.Core.Utilities;

namespace PeopleMatcher.App
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            initialiseLogging();
            InitializeComponent();
        }

        static void initialiseLogging()
        {
            Fluently.With(LogManager.GetRepository() as Hierarchy)
                .Do(repo => repo.Root.AddAppender(
                    Fluently.With(new DebugAppender())
                        .Do(appender => appender.Layout = new SimpleLayout())
                        .Do(appender => appender.ActivateOptions())
                        .Done()))
                .Do(repo => repo.Configured = true)
                .Do(repo => repo.Root.Level = Level.All)
                .Done();
        }
    }
}
