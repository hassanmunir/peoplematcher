﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace PeopleMatcher.App.Behaviours
{
    public class MultiSelectBehaviour : Behavior<DataGrid>
    {
        public static readonly DependencyProperty SelectedItemsProperty = DependencyProperty.Register(
            "SelectedItems",
            typeof (IList<object>),
            typeof (MultiSelectBehaviour),
            new FrameworkPropertyMetadata(null) {BindsTwoWayByDefault = true});

        public IList<object> SelectedItems
        {
            get { return (IList<object>) GetValue(SelectedItemsProperty); }
            set { SetValue(SelectedItemsProperty, value); }
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.SelectionChanged += onSeletionChanged;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            if (AssociatedObject != null)
            {
                AssociatedObject.SelectionChanged -= onSeletionChanged;
            }
        }

        void onSeletionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems != null && e.AddedItems.Count > 0 && SelectedItems != null)
            {
                foreach (var obj in e.AddedItems)
                    SelectedItems.Add(obj);
            }

            if (e.RemovedItems != null && e.RemovedItems.Count > 0 && SelectedItems != null)
            {
                foreach (var obj in e.RemovedItems)
                    SelectedItems.Remove(obj);
            }
        }
    }
}