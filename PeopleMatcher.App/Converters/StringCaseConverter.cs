﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace PeopleMatcher.App.Converters
{
    public class StringCaseConverter : IValueConverter
    {
        public StringConversionType ConvertStringTo { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var s = value as string;
            if (s == null) throw new ArgumentOutOfRangeException(nameof(value));

            switch (ConvertStringTo)
            {
                case StringConversionType.UpperCase:
                    return s.ToUpper();
                case StringConversionType.LowerCase:
                    return s.ToLower();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }

    public enum StringConversionType
    {
        UpperCase,
        LowerCase
    }
}