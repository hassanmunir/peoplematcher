using System.Collections.Generic;
using PeopleMatcher.Core;
using PeopleMatcher.Core.Models;
using PeopleMatcher.Utilities;

namespace PeopleMatcher.App.Mocks
{
    class MockPeopleRepository : IPeopleRepository
    {
        readonly List<Person> _people;

        public MockPeopleRepository()
        {
            _people = new List<Person>(FakePersonFactory.Create(10));
        }

        public OperationResult Add(Person person)
        {
            _people.Add(person);
            return new OperationResult(Status.Successful, $"Added {person}");
        }

        public IEnumerable<Person> GetAll()
        {
            return _people;
        }
    }
}