﻿using System;
using System.Collections.Generic;
using System.Linq;
using PeopleMatcher.Core;
using PeopleMatcher.Core.MatchProvider;
using PeopleMatcher.Core.Models;
using PeopleMatcher.Core.Rules;

namespace PeopleMatcher.App.Mocks
{
    internal class MockMatchesRepository : IMatchRepository
    {
        readonly List<Match> _matches;

        public MockMatchesRepository(IPeopleRepository peopleRepository)
        {
            _matches = new List<Match>();
            var matchProvider = new WeightedMatchProvider(new RandomRule(), 0.5);
            _matches = matchProvider
                .GetMatches(peopleRepository.GetAll().ToArray(), new List<Match>())
                .ToList();
        }

        public OperationResult Add(Match match)
        {
            _matches.Add(match);
            return new OperationResult(Status.Successful, $"Addded {match}");
        }

        public IEnumerable<Match> GetAll()
        {
            return _matches;
        }
    }
}