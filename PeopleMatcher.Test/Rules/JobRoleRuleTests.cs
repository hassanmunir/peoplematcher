﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using PeopleMatcher.Core.Models;
using PeopleMatcher.Core.Rules;

namespace PeopleMatcher.Test.Rules
{
    [TestFixture]
    public class JobRoleRuleTests : RuleTests
    {
        
        // ReSharper disable once UnusedMethodReturnValue.Local
        List<Tuple<JobRole, JobRole, double>> getTestData()
        {
            return new List<Tuple<JobRole, JobRole, double>>
            {
                Tuple.Create(JobRole.BusinessAnalyst, JobRole.BusinessAnalyst, 1.0),
                Tuple.Create(JobRole.BusinessAnalyst, JobRole.Developer, 0.0),
                Tuple.Create(JobRole.BusinessAnalyst, JobRole.BusinessAnalyst | JobRole.ProjectManager, 1.0),
                Tuple.Create(JobRole.BusinessAnalyst, JobRole.ProjectManager | JobRole.BusinessAnalyst, 1.0)
            };
        }

        [Test]
        [TestCaseSource(nameof(getTestData))]
        public void GetWeight(Tuple<JobRole, JobRole, double> parameters)
        {
            var people = TestUtilities.GetPeople(2, parameters.Item1, parameters.Item2);
            TestCorrectWeight(new JobRoleRule(1), people[0], people[1], parameters.Item3);
        }
    }
}