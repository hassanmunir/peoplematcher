﻿using System;
using System.Collections.Generic;
using System.Linq;
using FakeItEasy;
using NUnit.Framework;
using PeopleMatcher.Core;
using PeopleMatcher.Core.Extensions;
using PeopleMatcher.Core.Models;
using PeopleMatcher.Core.Rules;

namespace PeopleMatcher.Test.Rules
{
    [TestFixture]
    public class MatchEngineTests
    {
        static void assertMatch(Match match, Rule rule, double expectedWeight, int repeatNumOfTimes)
        {
            Assert.IsNotNull(match);
            Assert.AreEqual(expectedWeight, match.Weight);
            A.CallTo(() => rule.GetWeight(null, null))
                .WithAnyArguments()
                .MustHaveHappened(Repeated.Exactly.Times(repeatNumOfTimes));
        }

        static IEnumerable<double> getValidWeightRange()
        {
            for (double start = 0; start < 1.0; start += 0.1)
            {
                yield return start;
            }
        }

        [Test]
        public void GetMatch_AboveWeightThreshold()
        {
            Person person;
            IList<Person> people;
            TestUtilities.GetPersonAndPeople(out person, out people, 10);

            var rulesEngine = A.Fake<Rule>();
            A.CallTo(() => rulesEngine.GetWeight(null, null)).WithAnyArguments().Returns(1);

            var matcher = new MatchEngine(rulesEngine, 0.95);
            var match = matcher.GetMatch(person, people);

            assertMatch(match, rulesEngine, 1, 1);
        }

        [Test]
        public void GetMatch_BelowWeightThreshold()
        {
            Person person;
            IList<Person> people;
            TestUtilities.GetPersonAndPeople(out person, out people, 10);

            var rulesEngine = A.Fake<Rule>();
            A.CallTo(() => rulesEngine.GetWeight(person, person)).WithAnyArguments().Returns(0.35);

            var matcher = new MatchEngine(rulesEngine, 0.95);
            var match = matcher.GetMatch(person, people);

            assertMatch(match, rulesEngine, 0.35, 10);
        }

        [Test]
        public void GetMatch_MixedWeightThresholds()
        {
            Person person;
            IList<Person> people;
            TestUtilities.GetPersonAndPeople(out person, out people, 5);

            var rulesEngine = A.Fake<Rule>();
            A.CallTo(() => rulesEngine.GetWeight(person, person))
                .WithAnyArguments()
                .ReturnsNextFromSequence(0.1, 0.99, 0.5, 0.95, 0.7);

            var matcher = new MatchEngine(rulesEngine, 0.95);
            var match = matcher.GetMatch(person, people);

            assertMatch(match, rulesEngine, 0.99, 2);
        }

        [Test]
        public void GetMatch_NoPeople()
        {
            Person person;
            IList<Person> people;
            TestUtilities.GetPersonAndPeople(out person, out people, 0);

            var rulesEngine = A.Fake<Rule>();
            var matcher = new MatchEngine(rulesEngine, 0.95);
            var match = matcher.GetMatch(person, people);

            Assert.IsNull(match);
        }

        [Test]
        public void GetMatch_WeightThresholdInsideRange()
        {
            Person person;
            IList<Person> people;
            TestUtilities.GetPersonAndPeople(out person, out people, 10);

            var rulesEngine = A.Fake<Rule>();
            A.CallTo(() => rulesEngine.GetWeight(null, null))
                .WithAnyArguments()
                .ReturnsNextFromSequence(getValidWeightRange().ToArray());

            var matcher = new MatchEngine(rulesEngine, 0.95);
            Assert.DoesNotThrow(() => matcher.GetMatch(person, people));
        }

        [Test]
        public void GetMatch_WeightThresholdOutsideRange()
        {
            Person person;
            IList<Person> people;
            TestUtilities.GetPersonAndPeople(out person, out people, 10);

            var rulesEngine = A.Fake<Rule>();
            A.CallTo(() => rulesEngine.GetWeight(null, null)).WithAnyArguments().ReturnsNextFromSequence(1.1, -0.01);

            var matcher = new MatchEngine(rulesEngine, 0.95);
            Assert.Throws<ArgumentException>(() => matcher.GetMatch(person, people));
            Assert.Throws<ArgumentException>(() => matcher.GetMatch(person, people));
        }

        [Test]
        public void MatchEngine_WeightThresholdInsideRange()
        {
            var rulesEngine = A.Fake<Rule>();
            getValidWeightRange().ForEach(w => Assert.DoesNotThrow(() => new MatchEngine(rulesEngine, w)));
        }

        [Test]
        public void MatchEngine_WeightThresholdOutsideRange()
        {
            var rulesEngine = A.Fake<Rule>();
            Assert.Throws<ArgumentException>(() => new MatchEngine(rulesEngine, 1.1));
            Assert.Throws<ArgumentException>(() => new MatchEngine(rulesEngine, -0.1));
        }
    }
}