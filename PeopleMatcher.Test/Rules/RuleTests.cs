﻿using NUnit.Framework;
using PeopleMatcher.Core.Models;
using PeopleMatcher.Core.Rules;

namespace PeopleMatcher.Test.Rules
{
    public class RuleTests
    {
        protected static void TestCorrectWeight(Rule rule, Person person, Person match, double expectedWeight)
        {
            var weight = rule.GetWeight(person, match);
            Assert.AreEqual(expectedWeight, weight);
        }
    }
}