﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using PeopleMatcher.Core.Models;
using PeopleMatcher.Core.Rules;

namespace PeopleMatcher.Test.Rules
{
    [TestFixture]
    public class RegionRuleTestsTests
    {
        static IEnumerable<TestCaseData> getTestPeople()
        {
            yield return new TestCaseData("New York", "New York")
                .SetDescription("Same location should not match")
                .Returns(0);
            yield return new TestCaseData("New York", "Raleigh")
                .SetDescription("Different locations should match")
                .Returns(1);
            yield return new TestCaseData("new york", "New York")
                .SetDescription("Case shouldn't matter")
                .Returns(0);
            yield return new TestCaseData("New York", "raleigh")
                .SetDescription("Case shouldn't matter")
                .Returns(1);
            yield return new TestCaseData("New York", "Banana")
                .SetDescription("Unmapped location should throw")
                .Throws(typeof (ApplicationException));
        }

        RegionRule _rule;

        [TestFixtureSetUp]
        public void SetUp()
        {
            _rule = new RegionRule(new LocationMapper());
        }

        [Test]
        [TestCaseSource(nameof(getTestPeople))]
        public double GetWeight(string location1, string location2)
        {
            var person = TestUtilities.GetPerson();
            person.Attributes.Add(PersonAttribute.Location, location1);
            var potentionalMatch = TestUtilities.GetPerson();
            potentionalMatch.Attributes.Add(PersonAttribute.Location, location2);

            return _rule.GetWeight(person, potentionalMatch);
        }
    }
}