﻿using System;
using FakeItEasy;
using NUnit.Framework;
using PeopleMatcher.Core;
using PeopleMatcher.Core.Rules;

namespace PeopleMatcher.Test.Rules
{
    [TestFixture]
    public class AggreateRuleTests
    {
        [Test]
        public void GetMatch_MultipleRules()
        {
            var interestRule = A.Fake<Rule>();
            const double interestWeight = 0.6;
            const double interestReturn = 0.75;
            interestRule.Weighting = interestWeight;
            A.CallTo(() => interestRule.GetWeight(null, null)).WithAnyArguments().Returns(interestReturn);

            var professionRule = A.Fake<Rule>();
            const double professionWeight = 0.9;
            const double professionReturn = 0.7;
            professionRule.Weighting = professionWeight;
            A.CallTo(() => professionRule.GetWeight(null, null)).WithAnyArguments().Returns(professionReturn);

            var locationRule = A.Fake<Rule>();
            const int locationWeight = 1;
            const double locationReturn = 0.8;
            locationRule.Weighting = locationWeight;
            A.CallTo(() => locationRule.GetWeight(null, null)).WithAnyArguments().Returns(locationReturn);

            const double refWeight =
                (interestWeight*interestReturn) + (professionWeight*professionReturn) + (locationWeight*locationReturn);
            var normalised = Utils.Normalise(0, interestWeight + professionWeight + locationWeight, refWeight);

            var aggreateRule = new AggregateRule(interestRule, locationRule, professionRule);
            var weight = aggreateRule.GetWeight(TestUtilities.GetPerson(), TestUtilities.GetPerson());

            Assert.IsFalse(weight > 1.0, $"Weight invalid: {weight}");
            Assert.IsFalse(weight < 0, $"Weight invalid: {weight}");
            Assert.AreEqual(normalised, weight);
        }

        [Test]
        public void GetMatch_SingleRule()
        {
            var fakeRule = A.Fake<Rule>();
            fakeRule.Weighting = 0.75;
            A.CallTo(() => fakeRule.GetWeight(null, null)).WithAnyArguments().Returns(1);

            Assert.Throws<ArgumentException>(() => new AggregateRule(fakeRule));
        }
    }
}