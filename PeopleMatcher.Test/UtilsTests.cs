﻿using System;
using NUnit.Framework;
using PeopleMatcher.Core;

namespace PeopleMatcher.Test
{
    [TestFixture]
    public class UtilsTests
    {
        [Test]
        public void Normalise_ValueGreaterThanMax()
        {
            Assert.Throws<ArgumentException>(() => Utils.Normalise(1, 2.5, 3));
        }

        [Test]
        public void Normalise_ValueIsZero()
        {
            Assert.AreEqual(0, Utils.Normalise(0, 2.5, 0));
        }

        [Test]
        public void Normalise_ValueLessThanMin()
        {
            Assert.Throws<ArgumentException>(() => Utils.Normalise(1, 2.5, -1));
        }
    }
}