﻿using System;
using System.Collections.Generic;
using Faker;
using Faker.Selectors;
using PeopleMatcher.Core.Extensions;
using PeopleMatcher.Core.Models;
using PeopleMatcher.Core.Properties;

namespace PeopleMatcher.Test
{
    public static class TestUtilities
    {
        [NotNull]
        public static Person GetPerson()
        {
            var faker = getPersonFaker();
            return faker.Generate();
        }

        [NotNull]
        public static IList<Person> GetPeople(int count, JobRole jobRole = JobRole.Unspcified,
            JobRole preference = JobRole.Unspcified)
        {
            var faker = getPersonFaker();
            var people = faker.Generate(count);
            if (jobRole != JobRole.Unspcified) people.ForEach(person => person.Attributes.Add(PersonAttribute.JobRole, jobRole));
            if (preference != JobRole.Unspcified) people.ForEach(person => person.Attributes.Add(PersonAttribute.MatchPreference, preference));
            return people;
        }

        public static void GetPersonAndPeople(out Person person, out IList<Person> people, int count)
        {
            person = GetPerson();
            people = GetPeople(count);
        }

        [NotNull]
        static Fake<Person> getPersonFaker()
        {
            var faker = new Fake<Person>();
            faker.AddSelector(new AttributesSelector());
            return faker;
        }
    }

    internal class AttributesSelector : TypeSelectorBase<Dictionary<PersonAttribute, object>>
    {
        public override Dictionary<PersonAttribute, object> Generate()
        {
            return new Dictionary<PersonAttribute, object>();
        }
    }

    internal class JobRoleSelector : TypeSelectorBase<JobRole>
    {
        readonly Random _random = new Random();

        public override JobRole Generate()
        {
            var values = Enum.GetValues(typeof (JobRole));
            var num = _random.Next(0, values.Length - 1);

            return (JobRole) values.GetValue(num);
        }
    }
}