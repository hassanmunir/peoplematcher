﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using NSubstitute;
using NUnit.Framework;
using PeopleMatcher.Core;
using PeopleMatcher.Core.Models;

namespace PeopleMatcher.Test
{
    [TestFixture]
    public class PeopleRepositoryTests
    {
        IPersistor<Person> _persistor;
        IPeopleRepository _repository;

        [SetUp]
        public void Setup()
        {
            _persistor = Substitute.For<IPersistor<Person>>();
            _repository = new PeopleRepository(_persistor);
        }



        [Test]
        public void AddingPersonShouldAppendToPersistorAndReturnSuccess()
        {
            var person = new Person("Hassan", "Munir");
            _persistor.Retrieve().Returns(Enumerable.Empty<Person>());

            var result = _repository.Add(person);
            _persistor.Received(1).Append(person);
            
            Assert.AreEqual(Status.Successful, result.Status);
            Assert.AreEqual("Munir, Hassan added successfully.", result.Message);
        }


        [Test]
        public void AddingDuplicatePersonShouldFail()
        {
            var person = new Person("Hassan", "Munir");
            var people = TestUtilities.GetPeople(10);
            people.Add(person);
            _persistor.Retrieve().Returns(people);

            var result = _repository.Add(person);

            Assert.AreEqual(Status.Failed, result.Status);
            Assert.AreEqual("Munir, Hassan already exists.", result.Message);

            _persistor.DidNotReceive().Append(person);
        }

    }
}