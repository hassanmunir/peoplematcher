﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CommandLine;
using JetBrains.Annotations;
using log4net;
using log4net.Appender;
using log4net.Core;
using log4net.Layout;
using log4net.Repository.Hierarchy;
using PeopleMatcher.Core;
using PeopleMatcher.Core.Extensions;
using PeopleMatcher.Core.MatchProvider;
using PeopleMatcher.Core.Models;
using PeopleMatcher.Core.Rules;
using PeopleMatcher.Core.Utilities;
using YamlDotNet.Serialization;

namespace PeopleMatcher
{
    internal class Program
    {
        static readonly ILog Log = LogManager.GetLogger(typeof (Program));

        public static void Main(string[] args)
        {
            var programArgs = new ProgramArgs();
            if (!Parser.Default.ParseArguments(args, programArgs))
            {
                promptForKeyPress();
                return;
            }

            Trace.Listeners.Add(new ConsoleTraceListener(true));
            configureLogging();

            Configuration config;
            if (!tryGetConfiguration(programArgs.ConfigPath, out config))
            {
                Log.Error($"Could not parse {programArgs.ConfigPath}");
                promptForKeyPress();
                return;
            }

            // Create service providers
            var personSerializer = new PersonSerializer();
            var matchSerializer = new MatchSerializer(personSerializer);

            var unmatchedPersonPersistor = new PlainTextFilePersitor<Person>(
                personSerializer,
                Path.Combine(config.OutputFileDirectory, "UnmatchedPeople.txt"));

            var existingMatchesPersistor = new PlainTextFilePersitor<Match>(
                matchSerializer,
                Path.Combine(config.OutputFileDirectory, "Matches.txt"));

            var newMatchesPersistor = new PlainTextFilePersitor<Match>(
                matchSerializer,
                Path.Combine(config.OutputFileDirectory, "NewMatches.txt"));

            var peopleRepository = new CsvPeopleWithAttributesRepository() {FilePaths = new[] {@"TestInput\People.txt"}};

            var matchRepository = new MatchRepository(existingMatchesPersistor);
            
            if (programArgs.AddPerson)
            {
                addUser(peopleRepository);
                return;
            }

            if (programArgs.AddMatch)
            {
                addMatch(matchRepository);
                return;
            }

            var people = peopleRepository.GetAll().Randomize().ToList();

            var existingMatches = getExistingMatches(existingMatchesPersistor);

            var newMatches = config.MatchProvider
                .GetMatches(people.ToArray(), existingMatches)
                .ToList();

            //var newMatches = matchProvider.GetMatches(people.ToArray(), existingMatches).ToList();
            var unmatchedPeople = getUnmatchedPeople(newMatches, people);

            Trace.WriteLine(string.Empty);
            Trace.WriteLine($"Generated {newMatches.Count()} matches for {people.Count} people");

            Trace.WriteLine(string.Empty);
            newMatches.TraceDetails("All generated matches");

            Trace.WriteLine(string.Empty);
            unmatchedPeople.TraceDetails("Unmatched people");

            existingMatchesPersistor.Save(existingMatches);
            newMatchesPersistor.Save(newMatches);
            unmatchedPersonPersistor.Save(unmatchedPeople);

            promptForKeyPress();
        }

        static void addMatch(IMatchRepository matchRepository)
        {
            Console.WriteLine("Enter the following information:");

            Person first, second;
            {
                Console.Write("First persons first name: ");
                var firstName = Console.ReadLine();
                Console.Write("First persons last name: ");
                var lastName = Console.ReadLine();
                first = new Person(firstName,lastName);
            }

            {
                Console.Write("Second persons first name: ");
                var firstName = Console.ReadLine();
                Console.Write("Second persons last name: ");
                var lastName = Console.ReadLine();
                second = new Person(firstName, lastName);
            }

            var result = matchRepository.Add(new Match(first, second));

            Console.Write(result.Message);
        }

        static void addUser(IPeopleRepository peopleRepository)
        {
            Console.WriteLine("Enter the following information:");

            Console.Write("First Name: ");

            var firstName = Console.ReadLine();
            
            Console.Write("Last Name: ");
            var lastName = Console.ReadLine();
            var person = new Person(firstName, lastName);

            var result = peopleRepository.Add(person);

            Console.Write(result.Message);
        }

        static void configureLogging()
        {
            Fluently.With(LogManager.GetRepository() as Hierarchy)
                .Do(repo => repo.Root.AddAppender(
                    Fluently.With(new ColoredConsoleAppender())
                        .Do(appender => appender.Layout = new SimpleLayout())
                        .Do(appender => appender.ActivateOptions())
                        .Done()))
                .Do(repo => repo.Configured = true)
                .Do(repo => repo.Root.Level = Level.All)

                .Done();
        }

        /// <summary>
        ///     Tries to the get configuration.
        /// </summary>
        /// <param name="filePath">Program arguments.</param>
        /// <param name="config">The configuration.</param>
        /// <returns></returns>
        static bool tryGetConfiguration([NotNull]string filePath, out Configuration config)
        {
            config = null;

            try
            {
                var deserializer = new Deserializer();
                deserializer.RegisterTagMapping("!CompositeMatchProvider", typeof (CompositeMatchProvider));
                deserializer.RegisterTagMapping("!RandomMatchProvider", typeof (RandomMatchProvider));
                deserializer.RegisterTagMapping("!WeightedMatchProvider", typeof (WeightedMatchProvider));
                deserializer.RegisterTagMapping("!AggregateRule", typeof (AggregateRule));
                deserializer.RegisterTagMapping("!RandomRule", typeof (RandomRule));
                deserializer.RegisterTagMapping("!JobRoleRule", typeof (JobRoleRule));
                deserializer.RegisterTagMapping("!YearGroupRule", typeof (YearGroupRule));
                deserializer.RegisterTagMapping("!BackupMatchProvider", typeof (BackupMatchProvider));
                deserializer.RegisterTagMapping("!CsvPeopleWithAttributesRepository", typeof (CsvPeopleWithAttributesRepository));

                config = deserializer.Deserialize<Configuration>(new StreamReader(filePath));
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return false;
            }
        }

        /// <summary>
        ///     Gets the existing matches.
        /// </summary>
        /// <param name="existingMatchesPersistor">The existing matches persistor.</param>
        /// <returns>A <c>IList</c> of <c>existing matches</c></returns>
        static IList<Match> getExistingMatches(IPersistor<Match> existingMatchesPersistor)
        {
            var existingMatches = existingMatchesPersistor.Retrieve();
            return existingMatches.ToList();
        }

        static IList<Person> getUnmatchedPeople(IEnumerable<Match> matches, IEnumerable<Person> people)
        {
            var matchedPeople = new List<Person>();
            foreach (var match in matches)
            {
                matchedPeople.Add(match.FirstPerson);
                matchedPeople.Add(match.SecondPerson);
            }

            return people.Except(matchedPeople).ToList();
        }

        /// <summary>
        ///     Prompts and waits for a key press
        /// </summary>
        static void promptForKeyPress()
        {
            Console.WriteLine("Press any key to exit..");
            Console.ReadLine();
        }
    }
}