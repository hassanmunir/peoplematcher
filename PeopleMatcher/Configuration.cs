﻿using JetBrains.Annotations;
using PeopleMatcher.Core;
using PeopleMatcher.Core.MatchProvider;

namespace PeopleMatcher
{
    public class Configuration
    {
        public PeopleRepository PeopleRepository { get; set; }
        /// <summary>
        ///     Gets the file path where the people information is stored
        /// </summary>
        public string PeopleInputFilePath { get; [UsedImplicitly] private set; }

        /// <summary>
        ///     Gets the directory where all the output file paths are going to be stored
        /// </summary>
        public string OutputFileDirectory { get; [UsedImplicitly] private set; }

        /// <summary>
        ///     Gets the match providers that will be used for matching people
        /// </summary>
        public IMatchProvider MatchProvider { get; [UsedImplicitly] private set; }
    }
}