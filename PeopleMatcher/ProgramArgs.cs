using System.Net.WebSockets;
using CommandLine;
using CommandLine.Text;

namespace PeopleMatcher
{
    class ProgramArgs
    {
        [Option('c', "config", Required = true, HelpText = "Path to the yaml configuration file")]
        public string ConfigPath { get; set; }

        [Option('p', "newperson", Required = false, DefaultValue = false, HelpText = "Interactively add a new person.")]
        public bool AddPerson { get; set; }

        [Option('m', "newmatch", Required = false, DefaultValue = false, HelpText = "Interactively add a new match.")]
        public bool AddMatch { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this, current => HelpText.DefaultParsingErrorsHandler(this, current));
        }

    }
}