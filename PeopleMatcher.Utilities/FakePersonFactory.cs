﻿using System;
using System.Collections.Generic;
using Faker;
using Faker.Selectors;
using PeopleMatcher.Core.Extensions;
using PeopleMatcher.Core.Models;
using PeopleMatcher.Core.Properties;

namespace PeopleMatcher.Utilities
{
    public static class FakePersonFactory
    {
        [NotNull]
        public static Person Create()
        {
            var faker = getPersonFaker();
            return faker.Generate();
        }

        [NotNull]
        public static IList<Person> Create(int count, JobRole jobRole = JobRole.Unspcified,
            JobRole preference = JobRole.Unspcified)
        {
            var faker = getPersonFaker();
            var people = faker.Generate(count);
            if (jobRole != JobRole.Unspcified) people.ForEach(person => person.Attributes.Add(PersonAttribute.JobRole, jobRole));
            if (preference != JobRole.Unspcified) people.ForEach(person => person.Attributes.Add(PersonAttribute.MatchPreference, preference));
            return people;
        }

        [NotNull]
        static Fake<Person> getPersonFaker()
        {
            var faker = new Fake<Person>();
            faker.AddSelector(new EnumSelector<JobRole>());
            return faker;
        }
    }

    internal class EnumSelector<TEnum> : TypeSelectorBase<TEnum>
    {
        readonly Random _random = new Random();

        public override TEnum Generate()
        {
            var values = Enum.GetValues(typeof(TEnum));
            var num = _random.Next(0, values.Length - 1);

            return (TEnum)values.GetValue(num);
        }
    }
}