﻿using System.Collections.Generic;
using PeopleMatcher.Core.Models;

namespace PeopleMatcher.Core
{
    public interface IPeopleRepository
    {
        OperationResult Add(Person person);
        IEnumerable<Person> GetAll();
    }
}