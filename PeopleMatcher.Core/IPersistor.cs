using System.Collections.Generic;

namespace PeopleMatcher.Core
{
    public interface IPersistor<TModel>
    {
        void Save(TModel data);
        void Save(IEnumerable<TModel> data);
        IEnumerable<TModel> Retrieve();
        void Append(TModel data);
        void Append(IEnumerable<TModel> data);
    }
}