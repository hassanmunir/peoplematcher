﻿namespace PeopleMatcher.Core.Models
{
    public enum Region
    {
        NewYork,
        Raleigh,
        Switzerland,
        Poland,
        India
    }
}