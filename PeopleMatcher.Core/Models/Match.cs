﻿using System;
using System.Globalization;
using PeopleMatcher.Core.Properties;

namespace PeopleMatcher.Core.Models
{
    public class Match : IEquatable<Match>, IFormattable
    {
        public Match([NotNull] Person firstPerson, [NotNull] Person secondPerson)
        {
            if (Equals(firstPerson, secondPerson))
                throw new ArgumentException("Cannot make a match between the same people!");

            FirstPerson = firstPerson;
            SecondPerson = secondPerson;
        }

        public Person FirstPerson { get; }
        public Person SecondPerson { get; }
        public double Weight { get; set; }

        public bool Equals(Match other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return
                (FirstPerson.Equals(other.FirstPerson) || FirstPerson.Equals(other.SecondPerson)) &&
                (SecondPerson.Equals(other.SecondPerson) || SecondPerson.Equals(other.FirstPerson));
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Match) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (FirstPerson.GetHashCode()*397) ^ SecondPerson.GetHashCode();
            }
        }

        public string ToString(string format, IFormatProvider formatProvider)
        {
            if (string.IsNullOrEmpty(format)) format = "G";
            
            switch (format.ToUpperInvariant())
            {
                case "G":
                case "S":
                    return $"{FirstPerson,-25}{SecondPerson,-25}";
                case "D":
                    return $"{FirstPerson,-25}{SecondPerson,-25}{Weight,-25}";
                default:
                    throw new FormatException($"The {format} format string is not surpported");
            }
        }

        public override string ToString() => this.ToString("G", CultureInfo.CurrentCulture);
    }
}