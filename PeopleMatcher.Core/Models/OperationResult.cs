﻿namespace PeopleMatcher.Core.Models
{
    public class OperationResult
    {
        public OperationResult(Status status, string message)
        {
            Status = status;
            Message = message;
        }

        public Status Status { get; private set; }
        public string Message { get; private set; }
    }
}