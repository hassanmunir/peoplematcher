﻿using System;
using System.Collections.Generic;

namespace PeopleMatcher.Core.Models
{
    public enum PersonAttribute
    {
        Location,
        JobRole,
        MatchPreference,
        YearGroup
    }
}