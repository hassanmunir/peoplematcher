﻿namespace PeopleMatcher.Core.Models
{
    public enum Status
    {
        Successful,
        Failed,
        Fatal
    }
}