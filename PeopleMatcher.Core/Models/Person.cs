﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;

namespace PeopleMatcher.Core.Models
{
    public class Person : IEquatable<Person>, IFormattable
    {
        public Person()
        {
            Attributes = new Dictionary<PersonAttribute, object>();
        }

        public Person(string firstName, string lastName)
        {
            FirstName = firstName.Trim();
            LastName = lastName.Trim();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Dictionary<PersonAttribute, object> Attributes { get; set; }
        
        public string Email { get; set; }
        public string FullName => $"{FirstName} {LastName}";

        public bool Equals(Person other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(FirstName, other.FirstName) && string.Equals(LastName, other.LastName);
        }

        public string ToString(string format, IFormatProvider formatProvider)
        {
            if (string.IsNullOrEmpty(format)) format = "G";

            switch (format.ToUpperInvariant())
            {
                case "G":
                case "LF":
                    return $"{LastName}, {FirstName}";
                case "FL":
                    return $"{FirstName} {LastName}";
                default:
                    throw new FormatException($"The {format} format string is not surpported");
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Person) obj);
        }

        [SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
        public override int GetHashCode()
        {
            unchecked
            {
                return ((FirstName?.GetHashCode() ?? 0)*397) ^
                       (LastName?.GetHashCode() ?? 0);
            }
        }

        public override string ToString() => ToString("G", CultureInfo.CurrentCulture);

        public bool HasAttribute(PersonAttribute attribute)
        {
            return Attributes.ContainsKey(attribute);
        }
    }
}