﻿using System;

namespace PeopleMatcher.Core.Models
{
    [Flags]
    public enum JobRole
    {
        Unspcified = 0x0,
        BusinessAnalyst = 0x01,
        Developer = 0x02,
        ProjectManager = 0x04
    }
}