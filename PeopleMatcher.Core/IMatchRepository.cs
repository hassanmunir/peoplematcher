﻿using System.Collections.Generic;
using System.Linq;
using PeopleMatcher.Core.Models;

namespace PeopleMatcher.Core
{
    public interface IMatchRepository
    {
        OperationResult Add(Match match);
        IEnumerable<Match> GetAll();
    }

    public class MatchRepository : IMatchRepository
    {
              readonly IPersistor<Match> _persistor;

        public MatchRepository(IPersistor<Match> persistor)
        {
            _persistor = persistor;
        }

        public OperationResult Add(Match match)
        {
            var people = GetAll();
            if (people.Contains(match))
            {
                return new OperationResult(Status.Failed, $"{match} already exists.");
            }

            _persistor.Append(match);

            return new OperationResult(Status.Successful, $"{match} added successfully.");
        }

        public IEnumerable<Match> GetAll()
        {
            return _persistor.Retrieve();
        }
    }
}