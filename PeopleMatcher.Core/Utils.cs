using System;

namespace PeopleMatcher.Core
{
    public class Utils
    {
        public static double Normalise(double min, double max, double val)
        {
            if (val > max) throw new ArgumentException("Value can not be greated than the Max value");
            if (val < min) throw new ArgumentException("Value can not be smaller than the Min value");
            return (val - min)/(max - min);
        }

        /// <summary>
        ///     Checks if the weight is within the valid range
        /// </summary>
        /// <param name="weight">The weight.</param>
        /// <exception cref="System.ArgumentException">Thrown if weight is outside the valid range</exception>
        public static void ValidateWeightRange(double weight)
        {
            if (weight > 1.0 || weight < 0)
                throw new ArgumentException(
                    $"Weight value {weight} is invalid. Value must be smaller < 1.0 and > = 0");
        }
    }
}