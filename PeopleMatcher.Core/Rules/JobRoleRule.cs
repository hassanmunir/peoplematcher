﻿using PeopleMatcher.Core.Models;

namespace PeopleMatcher.Core.Rules
{
    public class JobRoleRule : Rule
    {
        public JobRoleRule()
        {
        }

        public JobRoleRule(double weighting)
        {
            Weighting = weighting;
        }

        public override double GetWeight(Person person, Person match)
        {
            var personPreference = (JobRole) person.Attributes[PersonAttribute.MatchPreference];
            var personJobRole = (JobRole) person.Attributes[PersonAttribute.JobRole];

            var matchPreference = (JobRole)match.Attributes[PersonAttribute.MatchPreference];
            var matchJobRole = (JobRole)match.Attributes[PersonAttribute.JobRole];

            if (personPreference == JobRole.Unspcified)
            {
                if (matchPreference.HasFlag(personJobRole)) return 1;
            }

            if (personPreference.HasFlag(matchJobRole)) return 1; // If my preference is other persons job
            if (personJobRole.HasFlag(matchPreference)) return 0.5; // If my job is other persons preference
            //if (person.JobRole.HasFlag(match.JobRole)) return 0.5;        // If my job is other persons job
            return 0;
        }
    }
}
