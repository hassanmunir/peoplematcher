﻿using PeopleMatcher.Core.Models;
using PeopleMatcher.Core.Properties;

namespace PeopleMatcher.Core.Rules
{
    public class RegionRule : Rule
    {
        public RegionRule()
        {
        }

        public RegionRule(LocationMapper locationMapper)
        {
            LocationMapper = locationMapper;
        }

        [NotNull]
        public LocationMapper LocationMapper { get; set; }

        public override double GetWeight(Person person, Person match)
        {
            var personRegion = LocationMapper.GetRegion((string) person.Attributes[PersonAttribute.Location]);
            var matchRegion = LocationMapper.GetRegion((string) match.Attributes[PersonAttribute.Location]);

            return personRegion == matchRegion ? 0 : 1;
        }
    }
}