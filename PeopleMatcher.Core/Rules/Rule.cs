using PeopleMatcher.Core.Models;
using PeopleMatcher.Core.Properties;

namespace PeopleMatcher.Core.Rules
{
    public abstract class Rule
    {
        double _weighting;

        /// <summary>
        ///     Gets or sets the weighting of this rule that can be used to calculate the final weight of the match.
        /// </summary>
        /// <value>
        ///     The weighting.
        /// </value>
        public double Weighting
        {
            get { return _weighting; }
            set
            {
                Utils.ValidateWeightRange(_weighting);
                _weighting = value;
            }
        }

        /// <summary>
        ///     Gets the weight of the possible match.
        /// </summary>
        /// <param name="person">The person.</param>
        /// <param name="match">The person to match with.</param>
        /// <returns>A number between 0 and 1</returns>
        public abstract double GetWeight([NotNull] Person person, [NotNull] Person match);
    }
}