﻿using System;
using System.Diagnostics;
using PeopleMatcher.Core.Models;

namespace PeopleMatcher.Core.Rules
{
    public class YearGroupRule : Rule
    {
        public YearGroupRule()
        {
        }

        public YearGroupRule(double weighting)
        {
            Weighting = weighting;
        }

        public override double GetWeight(Person person, Person match)
        {
            if (!person.HasAttribute(PersonAttribute.YearGroup))
            {
                Trace.TraceWarning($"{person} does not have a year group");
                return 0;
            }
            if (!match.HasAttribute(PersonAttribute.YearGroup))
            {
                Trace.TraceWarning($"{match} does not have a year group");
                return 0;
            }

            var personYearGroup = int.Parse(person.Attributes[PersonAttribute.YearGroup].ToString());
            var matchYearGroup = int.Parse(match.Attributes[PersonAttribute.YearGroup].ToString());
            var diff = Math.Abs(personYearGroup - matchYearGroup);

            return diff*Math.Exp(-diff)/Math.Exp(-1);
        }
    }
}