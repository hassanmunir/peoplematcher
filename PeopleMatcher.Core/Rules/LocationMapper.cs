using System;
using System.Collections.Generic;
using PeopleMatcher.Core.Models;

namespace PeopleMatcher.Core.Rules
{
    public class LocationMapper
    {
        readonly Dictionary<string, Region> _map;

        public LocationMapper()
        {
            _map = new Dictionary<string, Region>();

            Map("New York", Region.NewYork)
                .Map("Raleigh", Region.Raleigh)
                .Map("Wroclaw", Region.Poland)
                .Map("Mumbai", Region.India);
        }

        public LocationMapper Map(string location, Region region)
        {
            _map.Add(location.ToLower(), region);
            return this;
        }

        public Region GetRegion(string location)
        {
            location = location.ToLower();

            if (!_map.ContainsKey(location))
            {
                throw new ApplicationException($"Location {0} is not mapped to any regions.");
            }
            return _map[location];
        }
    }
}