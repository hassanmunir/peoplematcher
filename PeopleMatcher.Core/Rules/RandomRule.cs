﻿using System;
using PeopleMatcher.Core.Models;

namespace PeopleMatcher.Core.Rules
{
    /// <summary>
    ///     Rule for getting random weight. This can be used for randomly matching people or for adding randomness to the match
    ///     making process
    /// </summary>
    public class RandomRule : Rule
    {
        readonly Random _randomNumberGenerator = new Random();

        public RandomRule()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RandomRule" /> class.
        /// </summary>
        /// <param name="weighting">The weighting.</param>
        public RandomRule(double weighting)
        {
            Weighting = weighting;
        }

        /// <summary>
        ///     Gets a random weight
        /// </summary>
        /// <param name="person">The person.</param>
        /// <param name="match">The person to match with.</param>
        /// <returns>
        ///     A number between 0 and 1
        /// </returns>
        public override double GetWeight(Person person, Person match)
        {
            return _randomNumberGenerator.NextDouble();
        }
    }
}