using System;
using System.Collections.Generic;
using System.Linq;
using PeopleMatcher.Core.Models;

namespace PeopleMatcher.Core.MatchProvider
{
    public abstract class MatchProviderBase : IMatchProvider
    {
        IList<Person> _matchedPeople;
        IEnumerable<Person> _people;
        protected IList<Match> ExistingMatches;

        public IEnumerable<Match> GetMatches(Person[] people, IList<Match> existingMatches)
        {
            _people = people;
            _matchedPeople = new List<Person>();
            ExistingMatches = existingMatches;

            var matches = new List<Match>();

            foreach (var person in people)
            {
                Match match;
                if (tryMatch(person, out match))
                {
                    matches.Add(match);
                    registerMatch((match));
                }
            }

            return matches;
        }

        bool tryMatch(Person person, out Match match)
        {
            match = null;

            if (_matchedPeople.Contains(person))
            {
                return false;
            }

            var possibleMatches = GetEligibleMatches(person).ToList();

            if (possibleMatches.Count < 1)
            {
                return false;
            }

            var rand = new Random();
            var index = rand.Next(possibleMatches.Count - 1);

            match = possibleMatches[index];

            return true;
        }

        void registerMatch(Match match)
        {
            _matchedPeople.Add(match.FirstPerson);
            _matchedPeople.Add(match.SecondPerson);
            ExistingMatches.Add(match);
        }

        protected IEnumerable<Person> GetUnmatchedPeople()
        {
            return _people.Where(person => !_matchedPeople.Contains(person));
        }

        protected abstract IEnumerable<Match> GetEligibleMatches(Person person);
    }
}