﻿using System.Collections.Generic;
using PeopleMatcher.Core.Models;

namespace PeopleMatcher.Core.MatchProvider
{
    public interface IMatchProvider
    {
        IEnumerable<Match> GetMatches(
            Person[] people,
            IList<Match> existingMatches);
    }
}