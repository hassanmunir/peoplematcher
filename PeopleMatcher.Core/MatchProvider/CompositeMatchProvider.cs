﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using PeopleMatcher.Core.Extensions;
using PeopleMatcher.Core.Models;

namespace PeopleMatcher.Core.MatchProvider
{
    public class CompositeMatchProvider : IMatchProvider
    {
        public IMatchProvider[] MatchProviders { get; set; }

        public IEnumerable<Match> GetMatches(
            Person[] people,
            IList<Match> existingMatches)
        {
            IList<Match> matches = new List<Match>();

            if (!isInputValid(people)) return matches;

            IList<Person> matchedPeople = new List<Person>();

            foreach (var matchProvider in MatchProviders)
            {
                Trace.TraceInformation("Matching people using {0}", matchProvider.GetType().Name);

                var newMatches = matchProvider
                    .GetMatches(people, existingMatches)
                    .ToList();


                newMatches.ForEach(
                    match => registerMatch(match, ref matches, ref matchedPeople, ref existingMatches));

                people = people.Except(matchedPeople).ToArray();

                newMatches.TraceDetails(matchProvider.GetType().Name, "D");
            }

            return matches;
        }

        static void registerMatch(
            Match newMatch,
            ref IList<Match> newMatches,
            ref IList<Person> matchedPeople,
            ref IList<Match> existingMatches)
        {
            newMatches.Add(newMatch);
            matchedPeople.Add(newMatch.FirstPerson);
            matchedPeople.Add(newMatch.SecondPerson);
            existingMatches.Add(newMatch);
        }

        bool isInputValid(IEnumerable<Person> people)
        {
            if (!MatchProviders.Any())
            {
                Trace.TraceWarning("No MatchProvider specified.");
                return false;
            }

            if (!people.Any())
            {
                Trace.TraceWarning("No people specified.");
                return false;
            }

            return true;
        }
    }
}