﻿using System.Collections.Generic;
using System.Linq;
using PeopleMatcher.Core.Extensions;
using PeopleMatcher.Core.Models;

namespace PeopleMatcher.Core.MatchProvider
{
    public class BackupMatchProvider : MatchProviderBase
    {
        readonly List<Person> _backupPeople;

        public BackupMatchProvider()
        {
            _backupPeople = new List<Person>
            {
                new Person("Hassan", "Munir")
                {
                    Attributes = new Dictionary<PersonAttribute, object>
                    {
                        {PersonAttribute.JobRole, JobRole.Developer}
                    }
                },
                new Person("Jayne", "Vickery")
                {
                    Attributes = new Dictionary<PersonAttribute, object>
                    {
                        {PersonAttribute.JobRole, JobRole.BusinessAnalyst}
                    }
                },
                new Person("Mike", "Crestas")
                {
                    Attributes = new Dictionary<PersonAttribute, object>
                    {
                        {PersonAttribute.JobRole, JobRole.Developer}
                    }
                }
            };
        }

        protected override IEnumerable<Match> GetEligibleMatches(Person person)
        {
            return _backupPeople
                .Randomize()
                .Where(p => !p.Equals(person))
                .Select(p => new Match(person, p))
                .Where(match => !ExistingMatches.Contains(match));
        }
    }
}