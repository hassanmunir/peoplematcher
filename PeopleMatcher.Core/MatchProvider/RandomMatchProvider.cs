using System.Collections.Generic;
using System.Linq;
using PeopleMatcher.Core.Models;

namespace PeopleMatcher.Core.MatchProvider
{
    public class RandomMatchProvider : MatchProviderBase
    {
        protected override IEnumerable<Match> GetEligibleMatches(Person person)
        {
            var unmatchedPeople = GetUnmatchedPeople();

            return unmatchedPeople
                .Where(p => !p.Equals(person))
                .Select(p => new Match(person, p))
                .Where(match => !ExistingMatches.Contains(match));
        }
    }
}