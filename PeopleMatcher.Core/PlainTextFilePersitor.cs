﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PeopleMatcher.Core
{
    public class PlainTextFilePersitor<TModel> : IPersistor<TModel>
    {
        readonly string _filename;
        readonly ISerializer<TModel> _serializer;

        public PlainTextFilePersitor(ISerializer<TModel> serializer, string filename)
        {
            _serializer = serializer;
            _filename = filename;
        }

        public void Save(TModel data)
        {
            createDirectoryIfItDoesNotExist();

            using (var fs = new FileStream(_filename, FileMode.Create, FileAccess.Write))
            {
                using (var tx = new StreamWriter(fs))
                {
                    tx.WriteLine(_serializer.Serialize(data));
                }
            }
        }

        public void Save(IEnumerable<TModel> data)
        {
            createDirectoryIfItDoesNotExist();

            using (var fs = new FileStream(_filename, FileMode.Create, FileAccess.Write))
            {
                using (var tx = new StreamWriter(fs))
                {
                    foreach (var match in data)
                    {
                        tx.WriteLine(_serializer.Serialize(match));
                    }
                }
            }
        }

        public IEnumerable<TModel> Retrieve()
        {
            if (!File.Exists(_filename))
            {
                return Enumerable.Empty<TModel>();
            }

            var list = new List<TModel>();

            using (var fs = new FileStream(_filename, FileMode.Open, FileAccess.Read))
            {
                using (var stream = new StreamReader(fs))
                {
                    string line;
                    while ((line = stream.ReadLine()) != null)
                    {
                        list.Add(_serializer.Deserialize(line));
                    }
                }
            }

            return list;
        }

        public void Append(TModel data)
        {
            using (var fs = new FileStream(_filename, FileMode.Append, FileAccess.Write))
            {
                using (var tx = new StreamWriter(fs))
                {
                    tx.WriteLine(_serializer.Serialize(data));
                }
            }
        }

        public void Append(IEnumerable<TModel> data)
        {
            using (var fs = new FileStream(_filename, FileMode.Append, FileAccess.Write))
            {
                using (var tx = new StreamWriter(fs))
                {
                    foreach (var match in data)
                    {
                        tx.WriteLine(_serializer.Serialize(match));
                    }
                }
            }
        }

        void createDirectoryIfItDoesNotExist()
        {
            if (string.IsNullOrWhiteSpace(_filename))
            {
                throw new ArgumentNullException($"File name not specified for {GetType().FullName}");
            }
            var directory = Path.GetDirectoryName(_filename);

            if (string.IsNullOrWhiteSpace(directory))
            {
                return;
            }

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
        }
    }
}