﻿using System.Collections.Generic;
using System.Linq;
using PeopleMatcher.Core.Models;

namespace PeopleMatcher.Core
{
    public class PeopleRepository : IPeopleRepository
    {
        readonly IPersistor<Person> _persistor;

        public PeopleRepository(IPersistor<Person> persistor)
        {
            _persistor = persistor;
        }

        public OperationResult Add(Person person)
        {
            var people = GetAll();
            if (people.Contains(person))
            {
                return new OperationResult(Status.Failed, $"{person} already exists.");
            }

            _persistor.Append(person);

            return new OperationResult(Status.Successful, $"{person} added successfully.");
        }

        public IEnumerable<Person> GetAll()
        {
            return _persistor.Retrieve();
        }
    }
}