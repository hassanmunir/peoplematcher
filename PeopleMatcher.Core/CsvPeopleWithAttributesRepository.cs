﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using PeopleMatcher.Core.Extensions;
using PeopleMatcher.Core.Models;

namespace PeopleMatcher.Core
{
    public class CsvPeopleWithAttributesRepository : IPeopleRepository
    {
        public string[] FilePaths { get; set; }

        public OperationResult Add(Person person)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Person> GetAll()
        {
            var list = new List<Person>();

            foreach (var filePath in FilePaths)
            {
                list.AddRange(getPeople(filePath));
            }

            return list;
        }

        IEnumerable<Person> getPeople(string filePath)
        {
            var people = new List<Person>();

            using (var fs = new FileStream(filePath, FileMode.Open))
            using (var reader = new StreamReader(fs))
            using (var csvReader = new CsvReader(reader))
            {
                while (csvReader.Read())
                {
                    var line = csvReader.CurrentRecord;
                    var header = csvReader.FieldHeaders;

                    for (int i = 0; i < line.Length; i++)
                    {
                        line[i] = line[i].Trim();
                    }
                    for (int i = 0; i < header.Length; i++)
                    {
                        header[i] = header[i].Trim();
                    }

                    try
                    {
                        people.Add(getPerson(header, line));
                    }
                    catch (ApplicationException ex)
                    {
                        throw new AggregateException($"Error parsing {filePath}", ex);
                    }
                }
            }
            return people;
        }

        Person getPerson(string[] header, string[] line)
        {
            var personProperties = (typeof (Person)).GetProperties();

            var person = Activator.CreateInstance<Person>();
            var indicesProcessed = new int[personProperties.Length];

            for (var i = 0; i < personProperties.Length; i++)
            {
                var personProperty = personProperties[i];

                if (personProperty.Name == nameof(Person.Attributes)) continue;
                if(!personProperty.CanWrite) continue;

                var index = Array.IndexOf(header, personProperty.Name);
                if (index == -1)
                {
                    throw new ApplicationException(
                        $"CSV does not contain a column matching property name {personProperty.Name}.");
                }

                var value = line[index].Trim();
                personProperty.SetValue(person, value);

                indicesProcessed[i] = index;
            }

            var indicesToProcess = Enumerable
                .Range(0, header.Length)
                .Except(indicesProcessed)
                .ToArray();

            foreach (var t in indicesToProcess)
            {
                var column = header[t];
                var value = line[t].Trim();

                var allowedAttributes = Enum.GetNames(typeof (PersonAttribute)).ToArray();

                var attributeIndex = Array.IndexOf(allowedAttributes, column);

                if (attributeIndex == -1)
                {
                    throw new ApplicationException($"CSV column {column} is not an allowed attribute");
                }

                person
                    .Attributes
                    .Add(
                        (PersonAttribute) Enum.Parse(typeof (PersonAttribute), allowedAttributes[attributeIndex]),
                        value);
            }

            return person;
        }
    }
}