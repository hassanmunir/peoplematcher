using System;
using System.Collections.Generic;
using PeopleMatcher.Core.Properties;

namespace PeopleMatcher.Core.Extensions
{
    public static class ListExtensions
    {
        public static void Shuffle<T>([NotNull] this IList<T> list, [NotNull] Random rnd)
        {
            for (var i = 0; i < list.Count; i++)
                list.Swap(i, rnd.Next(i, list.Count));
        }

        public static void Swap<T>([NotNull] this IList<T> list, int i, int j)
        {
            var temp = list[i];
            list[i] = list[j];
            list[j] = temp;
        }
    }
}