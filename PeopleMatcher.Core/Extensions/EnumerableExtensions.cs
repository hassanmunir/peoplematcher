using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using PeopleMatcher.Core.Properties;

namespace PeopleMatcher.Core.Extensions
{
    public static class EnumerableExtensions
    {
        [NotNull]
        public static IEnumerable<T> Randomize<T>([NotNull] this IEnumerable<T> source)
        {
            var rnd = new Random();
            return source.OrderBy(arg => rnd.Next());
        }

        /// <summary>
        ///     Performs an action for each element in the enumeration
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumeration">The enumeration.</param>
        /// <param name="action">The action to perform.</param>
        public static void ForEach<T>([NotNull] this IEnumerable<T> enumeration, [NotNull] Action<T> action)
        {
            foreach (var item in enumeration)
            {
                action(item);
            }
        }

        /// <summary>
        ///     Prints an introductory message then prints all data indented by calling ToString() on all objects
        /// </summary>
        /// <typeparam name="T">The object to print details of.</typeparam>
        /// <param name="enumeration">The data.</param>
        /// ///
        /// <param name="titleText">The introduction message.</param>
        /// <param name="format"></param>
        public static void TraceDetails<T>([NotNull] this IEnumerable<T> enumeration, [NotNull] string titleText,
            string format = "G")
        {
            var list = enumeration.ToList();

            if (!list.Any()) return;

            Trace.WriteLine(titleText);
            Trace.Indent();
            foreach (var obj in list)
            {
                var formattable = obj as IFormattable;
                if (formattable != null)
                {
                    Trace.WriteLine(formattable.ToString(format, CultureInfo.CurrentCulture));
                }
                else
                {
                    Trace.WriteLine(obj);
                }
            }
            Trace.Unindent();
        }
    }
}