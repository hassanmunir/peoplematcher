﻿using System.Collections.Generic;
using System.Linq;
using PeopleMatcher.Core.Models;
using PeopleMatcher.Core.Properties;
using PeopleMatcher.Core.Rules;

namespace PeopleMatcher.Core
{
    public class MatchEngine
    {
        readonly Rule _rule;
        readonly double _weightThreshold;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MatchEngine" /> class.
        /// </summary>
        /// <param name="rule">The rule to use for generating matches.</param>
        /// <param name="weightThreshold">The weight threshold at which no further matches will be evaluated</param>
        public MatchEngine(Rule rule, double weightThreshold)
        {
            Utils.ValidateWeightRange(weightThreshold);
            _rule = rule;
            _weightThreshold = weightThreshold;
        }

        [CanBeNull]
        public Match GetMatch([NotNull] Person person, [NotNull] IList<Person> people)
        {
            if (people.Count == 0)
                return null;

            var matches = new List<Match>();

            foreach (var p in people)
            {
                var weight = _rule.GetWeight(person, p);

                Utils.ValidateWeightRange(weight);

                var match = new Match(person, p) {Weight = weight};
                if (weight >= _weightThreshold) return match;
                matches.Add(match);
            }

            return matches
                .OrderByDescending(m => m.Weight)
                .FirstOrDefault();
        }
    }
}