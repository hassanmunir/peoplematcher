﻿using System;
using System.Linq;
using PeopleMatcher.Core.Models;

namespace PeopleMatcher.Core
{
    public class PersonSerializer : ISerializer<Person>
    {
        public string Serialize(Person obj)
        {
            return obj.ToString();
        }

        public Person Deserialize(string str)
        {
            var split = str.Split(',');
            var names = split.Select(s => s.Trim()).ToArray();

            if (names.Count() != 2)
            {
                throw new ArgumentException("Invalid string");
            }

            return new Person(names[1], names[0]);
        }
    }
}